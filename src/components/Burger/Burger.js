import React from 'react'
import Ingredient from './Ingredient/Ingredient'
import './Burger.css'

const burger = props => {
  let transformedIngredients = Object.keys(props.ingredients)
    .map(igKey => {
      // console.log('>', props.ingredients[igKey], [...Array(props.ingredients[igKey])])
      return [...Array(props.ingredients[igKey])].map((_, i) => {
        // console.log('>>', igKey + i)
        return <Ingredient type={igKey} key={igKey + i} />
      })
    })
    .reduce((arr, el) => arr.concat(el), [])
  
  // console.log('>>>', transformedIngredients)

  if(transformedIngredients.length === 0) {
    transformedIngredients = <p> Please add Ingredient ..! </p>
  }

  return (
    <div className="Burger text-center">
      <Ingredient type="bread-top"></Ingredient>
      { transformedIngredients }
      <Ingredient type="bread-bottom"></Ingredient>
    </div>
  )
}

export default burger
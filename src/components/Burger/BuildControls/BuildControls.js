import React from 'react'
import BuildControl from './BuildControl/BuildControl'

const controls = [
  { label: 'Salad', type: 'salad'},
  { label: 'Bacon', type: 'bacon'},
  { label: 'Cheese', type: 'cheese'},
  { label: 'Meat', type: 'meat'}
]

const buildControls = props => (
  <div className="border text-center pt-1 pb-1">
    <div className="text-center border-bottom pt-2 pb-2"> Current Price : {props.totalPrice.toFixed(2)} </div>
    {controls.map(v => {
      return (
        <BuildControl 
          label={v.label}
          key={v.label}
          added={() => props.ingredientAdded(v.type)}
          removed={() => props.ingredientRemoved(v.type)}
          disabled={props.disabledInfo[v.type]}
        />
      )
    })}
    <div className="mt-2 pt-2 text-center border-top"> 
      <button 
        type="button"
        className="btn btn-success" 
        disabled={props.purchasable}
        onClick={props.ordered} 
      > ORDER NOW
      </button>
    </div>
  </div>
)

export default buildControls
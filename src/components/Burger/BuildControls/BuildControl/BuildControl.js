import React from 'react'

const buildControl = props => (
  <div className="container mb-2 pt-2">
    <div className="row">
      <div className="col-5 text-right">{props.label}</div>
      <div className="col text-left">
        <button 
          type="button"
          className="btn btn-primary ml-1 mr-1"
          onClick={props.added}
        > More
        </button>
        <button
         type="button"
         className="btn btn-warning ml-1 mr-1"
         onClick={props.removed}
         disabled={props.disabled}
        > Less {props.disabled}
        </button>
      </div>
    </div>
  </div>
)

export default buildControl
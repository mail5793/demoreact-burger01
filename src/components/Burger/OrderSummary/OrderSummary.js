import React, {Component} from 'react'
import Ax from '../../../hoc/Ax'

class OrderSummary extends Component {
  componentWillUpdate() {
    console.log('[orderSummary]componentWillUpdate')
  }

  render() {
    const ingredientSummary = Object.keys(this.props.ingredients)
    .map(k => {
      return <li key={k}><span style={{textTransform: 'capitalize'}}>{k}</span> : {this.props.ingredients[k]}</li>
    })

    return (
      <Ax>
        <h3>Your Order</h3>
        <p>A delisious burger with the following ingredients : </p>
        <ul>
          {ingredientSummary}
        </ul>
        <div className="border-top mb-2">Total Price : {this.props.totalPrice.toFixed(2)}</div>
        <div className="container">
          <div className="row">
            <div className="col-6">Continue Checkout ?</div>
            <div className="col-6 text-right">
              <button type="button" className="btn btn-success ml-2 mr-2" onClick={this.props.continueOrder}>Yes</button>
              <button type="button" className="btn btn-danger" onClick={this.props.orderClosed}>No</button>
            </div>
          </div>
        </div>
      </Ax>
    )
  }
}

export default OrderSummary
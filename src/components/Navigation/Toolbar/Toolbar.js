import React from 'react'
import Logo from '../../UI/Logo/Logo'
import NavigationItems from '../NavigationItems/NavigationItems'
import Hamburger from '../../UI/Hamburger/Hamburger'

import './Toolbar.css'

const toolbar = props => (
  <header className="toolbar">
    <Hamburger clicked={props.openedDrawer} />
    <Logo height="80%" />
    <nav className="desktop-only">
      <NavigationItems />
    </nav>
  </header>
)

export default toolbar
import React from 'react'
import NavigationItem from './NavigationItem/NavigationItem'

import './NavigationItems.css'

const navigationItems = () => (
  <ul className="navigation-items">
    <NavigationItem link="/" active> Burger Builder </NavigationItem> {/* boolean props */}
    <NavigationItem link="/"> Checkout </NavigationItem>
  </ul>  
)

export default navigationItems
import React from 'react'
import Logo from '../../UI/Logo/Logo'
import NavigationItems from '../NavigationItems/NavigationItems'
import Backdrop from '../../UI/Backdrop/Backdrop'

import './SideDrawer.css'

const sideDrawer = props => {
  return (
    <>
      <Backdrop show={props.open} clicked={props.closed} />
      <div className={props.open ? 'side-drawer open':'side-drawer close'}>
        <Logo height="10%" />
        <nav className="mt-2">
          <NavigationItems/>
        </nav>
      </div>
    </>
  )
}

export default sideDrawer
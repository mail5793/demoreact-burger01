import React, {Component} from 'react'
import Burger from '../../components/Burger/Burger'
import BuildControls from '../../components/Burger/BuildControls/BuildControls'
import Modal from '../../components/UI/Modal/Modal'
import OrderSummary from '../../components/Burger/OrderSummary/OrderSummary'
import Ax from '../../hoc/Ax'

const INGREDIENT_PRICE = {
  salad: 0.5,
  cheese: 0.4,
  meat: 1.3,
  bacon: 0.7
}

//statefull component
class BurgerBuilder extends Component {
  state = {
    ingredients: {
      salad: 0,
      bacon: 0,
      cheese: 0,
      meat: 0
    },
    totalPrice: 4,
    purchasable: false,
    purchasing: false
  }

  updatePurchaseState = (ingredients) => {
    // const ingredients = { ...this.state.ingredients }
    const sum = Object.keys(ingredients)
      .map(k => {
        return ingredients[k]
      })
      .reduce((sum, el) => {
        return sum + el
      }, 0) 

      this.setState({purchasable: sum > 0})
  }

  addIngredientHandler = (type) => {
    const oldCount = this.state.ingredients[type]
    const updatedCounted = oldCount + 1
    const updatedIngerdients = { ...this.state.ingredients }
    updatedIngerdients[type] = updatedCounted

    const priceAddition = INGREDIENT_PRICE[type]
    const oldPrice = this.state.totalPrice
    const newPrice = oldPrice + priceAddition

    this.setState({
      ingredients: updatedIngerdients,
      totalPrice: newPrice
    })
    this.updatePurchaseState(updatedIngerdients)
  }

  removeIngredientHandler = (type) => {
    const oldCount = this.state.ingredients[type]
    if(oldCount <= 0) return 
    const updatedCounted = oldCount - 1 
    const updatedIngerdients = { ...this.state.ingredients }
    updatedIngerdients[type] = updatedCounted

    const priceDeduction = INGREDIENT_PRICE[type]
    const oldPrice = this.state.totalPrice
    const newPrice = oldPrice - priceDeduction

    this.setState({
      ingredients: updatedIngerdients,
      totalPrice: newPrice
    })
    this.updatePurchaseState(updatedIngerdients)
  }

  purchaseHandler = () => {
    this.setState({purchasing: true})
  }

  purchaseCancelHandler = () => {
    this.setState({purchasing: false})
  }

  purchaseContinueHandler = () => {
    alert('you continue !')
  }

  render() {
    const disableInfo = {...this.state.ingredients}
    for(let key in disableInfo) {
      disableInfo[key] = disableInfo[key] <= 0
    }
    return (
      <Ax>
      {/* <> // => react fragment */}
        <Modal show={this.state.purchasing} modalClosed={this.purchaseCancelHandler}>
          <OrderSummary
            totalPrice={this.state.totalPrice} 
            ingredients={this.state.ingredients}
            orderClosed={this.purchaseCancelHandler}
            continueOrder={this.purchaseContinueHandler}
          />
        </Modal>
        <Burger ingredients={this.state.ingredients} />
        <BuildControls 
          disabledInfo={disableInfo}
          totalPrice={this.state.totalPrice}
          purchasable={!this.state.purchasable}
          ingredientAdded={this.addIngredientHandler} 
          ingredientRemoved={this.removeIngredientHandler}
          ordered={this.purchaseHandler}
        />
      {/* </> */}
      </Ax>
    )
  }
}

export default BurgerBuilder